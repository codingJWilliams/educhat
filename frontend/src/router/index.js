import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import config from '../../config.json';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/class/:class_id/channel/:channel_id',
    name: 'channel',
    component: () => import(/* webpackChunkName: "channel" */ '../views/Channel.vue')
  },
  {
    path: '/class/:class_id',
    name: 'class',
    component: () => import(/* webpackChunkName: "channel" */ '../views/Class.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import(/* webpackChunkName: "admin" */ '../views/Admin.vue')
  },
  {
    path: '/usersettings',
    name: 'usersettings',
    component: () => import(/* webpackChunkName: "usersettings" */ '../views/UserSettings.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

Vue.mixin({
  data: function() {
    return {
      get key() {
        return Vue.cookies.get("key");
      },
      get auth_api_url() {
        return config.backend_url + "/api/" + Vue.cookies.get("key");
      }
    }
  }
})

export default router
