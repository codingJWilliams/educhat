import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex';
import VueCookies from 'vue-cookies'
import axios from 'axios';
import VueChatScroll from 'vue-chat-scroll'
import config from '../config.json';
import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyDWaRzkKqk3dg9uS3WT78TSNveFteQdOPs",
  authDomain: "educhat-1b4d2.firebaseapp.com",
  databaseURL: "https://educhat-1b4d2.firebaseio.com",
  projectId: "educhat-1b4d2",
  storageBucket: "educhat-1b4d2.appspot.com",
  messagingSenderId: "590906664242",
  appId: "1:590906664242:web:5da7418d613b8e897ae32b",
  measurementId: "G-0CCJSYP7HP"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
const messaging = firebase.messaging();

// Foreground - manually show notification
messaging.onMessage(function (payload) {
  new Notification(payload.notification.title, payload.notification);
});

async function tryToGetToken() {
  try {
    const currentToken = await messaging.getToken();
    if (currentToken) {
      return currentToken;
    }
    else {
      const permission = Notification.requestPermission();
      if (permission === 'granted') return await tryToGetToken();
      else console.log("Permission denied.");
      return false
    }
  } catch (e) {
    return false
  }
}
Vue.use(VueChatScroll)


// The URL we should make API requests to
const BACKEND_URL = config.backend_url;

Vue.config.productionTip = false
// import modules
Vue.use(VueCookies);
Vue.use(Vuex);

// Make a store (like a database but client side and temporary)
const store = new Vuex.Store({
  state: { // Current state of the database
    count: 0,
    backend_url: BACKEND_URL,
    profileInfo: { loading: true },
    key: false
  },
  mutations: { // Functions to modify the databasse
    increment(state) { // Old test code, used to help me learn how the Vuex store works
      state.count++
    },
    setGlobalLoading(state, loading) {
      // Sets whether or not the whole app page should be loading
      state.globalLoading = loading;
    },
    updateProfileInfo(state, user) {
      // Updates when we fetch our profile info (name, email, etc) from the server
      state.profileInfo = user;
      state.profileInfo.loading = false;
    },
    setClassChannels(state, { class_id, channels }) {
      // Runs when we fetch the channels for a given class, from the server
      channels.loading = false;
      channels.forEach(c => { c.messages = { loading: true }; })
      // Find the class, by class_id, then set it's channels
      state.profileInfo.classes.find(c => c.id === class_id).channels = { loading: false, channels };
    },
    setChannelMessages(state, { class_id, channel_id, messages }) {
      // Runs when we fetch the messages for a given channel from the server

      // Find the class from the store, by class_id
      // Note: called `_class` because class is a reserved word and that was causing errors.
      var _class = state.profileInfo.classes.find(c => parseInt(c.id) === parseInt(class_id));
      if (!_class.channels.channels) return;

      // Find the channel in the class, and set it's messages
      _class.channels.channels.find(c => parseInt(c.id) === parseInt(channel_id)).messages = { loading: false, messages };
    }
  },
  actions: {
    async fetchProfileInfo({ commit, state }) {
      // Fetch our own profile and courses from the api
      let user = {};
      let profile_response = await axios.get(`${BACKEND_URL}/api/${state.key}/profile/me`);
      user.email = profile_response.data.email;
      user.name = profile_response.data.name;
      user.profile_pic = profile_response.data.picture;
      user.teacher = profile_response.data.teacher;
      user.dyslexiaMode = profile_response.data.dyslexiaMode;
      user.leadership = profile_response.data.leadership;
      // fetch courses
      let classes_response = await axios.get(`${BACKEND_URL}/api/${state.key}/courses`);
      user.classes = classes_response.data.map(c => { c.channels = { loading: true }; return c });

      const token = await tryToGetToken();
      if (token) await axios.post(`${BACKEND_URL}/api/${state.key}/notifications/add`, { token });

      commit("updateProfileInfo", user); // Call updateProfileInfo mutation
    },
    async updateChannels({ commit, state }, class_id) {
      // fetch the channels that are in a class
      let channels_response = await axios.get(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channels`);
      commit("setClassChannels", { class_id, channels: channels_response.data });
    },
    async updateKey({ dispatch, state }) {
      // set the key that we are using to talk to backend api
      state.key = key;
      // re-get our profile info
      if (key) dispatch("fetchProfileInfo");
    },
    async updateMessages({ commit, state }, { class_id, channel_id }) {
      // fetch messages for a channel, from the API
      var messages = (await axios.get(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel_id}/messages`)).data;
      commit("setChannelMessages", {
        class_id,
        channel_id,
        messages
      });
    },
    async sendMessage({ state }, { class_id, channel_id, content }) {
      // api request to send a message to a channel
      let response = await axios.post(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel_id}/message/send`, { content });
      if (response.data.error) alert(response.data.message);
    },
    async saveChannel({ state }, { class_id, channel }) {
      let response = await axios.post(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel.id}`, { channel });
      if (response.data.error) alert(response.data.message);
    },
    async deleteChannel({ state }, { class_id, channel }) {
      let response = await axios.delete(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel.id}`);
      if (response.data.error) alert(response.data.message);
    },
    async createChannel({ state }, { class_id, name, locked }) {
      // api request to send a message to a channel
      let response = await axios.post(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/create`, { name, type: locked });
      await store.dispatch("updateChannels", class_id);
      if (response.data.error) alert(response.data.message);
      return response.data.id;
    },
    async sendFileMessage({ state }, { class_id, channel_id, file_id }) {
      // api request to send a message to a channel
      let response = await axios.post(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel_id}/message/sendfile/${file_id}`);
      console.log(response.data);
      if (!response.data.success) alert(response.data.message);
    },
    async uploadFile({ state }, file) {
      let data = new FormData();
      data.append('name', 'upload');
      data.append('file', file);
      return await axios.post(`${BACKEND_URL}/cdn/upload`, data);
    },
    async uploadVoiceNote({ state }, blob) {
      let data = new FormData();
      data.append('file', blob, "voicenote.wav");
      return await axios.post(`${BACKEND_URL}/cdn/upload`, data);
    },
    async sendReport({ state }, body) {
      return await axios.post(`${BACKEND_URL}/api/${state.key}/report`, body);
    },
    async saveProfile({ state, dispatch }, profileInfo) {
      await axios.post(`${BACKEND_URL}/api/${state.key}/profile/me`, profileInfo);
      await dispatch("fetchProfileInfo");
    },
    async log({ state, dispatch }, message) {
      await axios.post(`${BACKEND_URL}/api/${state.key}/auditlog`, {
        message,
        time: Math.round(Date.now())
      });
    },
    async getlog({ state, dispatch }) {
      return (await axios.get(`${BACKEND_URL}/api/${state.key}/auditlog`)).data;
    },
    async changeNotifications({ state }, {class_id, channel_id, notifications}) {
      await axios.post(`${BACKEND_URL}/api/${state.key}/course/${class_id}/channel/${channel_id}/notifications`, {
        notifications
      });
    }
  },
  getters: {
    globalLoading(state) {
      // return whether or not the whole app should have a big loading icon
      return state.profileInfo.loading;
    },
    getClassById: (state) => (id) => {
      // given a class's id, return it's object
      return state.profileInfo.classes.find(_class => _class.id === id)
    },
    getChannelById: (state) => (class_id, channel_id) => {
      // given a class and a channel id, return the channel object
      var _class = store.getters.getClassById(class_id);
      if (!_class) return null;
      return _class.channels.channels.find(_channel => _channel.id == channel_id)
    }
  }
})

// Load the application
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


// start with no API key
var key = false;

// If we've just been redirected back to, by the backend, then it will redirect the user to `/logincallback/THEIR API KEY` 
if (router.currentRoute.path.startsWith("/logincallback/")) {
  // Therefore get the API key given
  key = router.currentRoute.path.split("/logincallback/")[1];
  // Set it as a cookie so it persists between page loads
  VueCookies.set("key", key);
  // and send the user to the homepage
  router.push({
    path: "/",
    force: true
  })
}
// if we still don't have a key, try and get it from the cookie
if (!key) {
  if (VueCookies.get("key")) key = VueCookies.get("key");
}

// if we still don't have a key then at this point just give up and make them sign in
if (!key) {
  router.push({
    path: "/login",
    force: true
  })
}

// Re-get profile info, so that we have the most up to date profile data
store.dispatch("updateKey", key);