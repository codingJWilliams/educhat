// Service worker for notifications, helps read notifications while app is not open

importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyDWaRzkKqk3dg9uS3WT78TSNveFteQdOPs",
    authDomain: "educhat-1b4d2.firebaseapp.com",
    databaseURL: "https://educhat-1b4d2.firebaseio.com",
    projectId: "educhat-1b4d2",
    storageBucket: "educhat-1b4d2.appspot.com",
    messagingSenderId: "590906664242",
    appId: "1:590906664242:web:5da7418d613b8e897ae32b",
    measurementId: "G-0CCJSYP7HP"
});
const messaging = firebase.messaging();
messaging.usePublicVapidKey("BGY3Qs-5CQa-ntr2JBEQt3zYCTQdiB0fWplKOyKX1Q8HH76ZPFL7Ky-bNKB0njPH6y9KutkQWdSbWnhpKMvXHlE");