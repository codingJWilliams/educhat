var express = require('express')
var router = express.Router()
var googleApi = require("../util/google-api");
var dbHelper = require("../util/db");
var bodyParser = require('body-parser');
const config = require("../config.json");

async function getRouter() {
  var orm = await dbHelper.getOrm();
  var User = orm.collections.user;
  var Session = orm.collections.session;
  var Channel = orm.collections.channel;
  var Message = orm.collections.message;
  var File = orm.collections.file;
  var Report = orm.collections.report;
  var Auditlog = orm.collections.auditlog;
  var NotificationDevice = orm.collections.notificationdevice;
  var NotificationPreference = orm.collections.notificationpreference;

  var notification = require("../util/notifications")(orm);

  async function validateMessage(course_id, message) {
    // Makes sure a message is valid before it gets put inside the database

    // Validate that the channel ID sent exists
    var channel = await Channel.findOne({ class: course_id, id: message.channel });
    if (!channel) return {
      success: false,
      error: "Channel Not Found"
    }; // If not, return an error

    // Validate Timestamp
    var now = Math.round(Date.now() / 1000);
    // Make sure the difference in time between the message timestamp, and now, is less than 2 seconds difference
    if (Math.abs(message.timestamp - now) > 2) return {
      success: false,
      error: "Invalid Timestamp"
    }; // If not, return an error

    // Validate Content
    if (!message.content.length) return {
      success: false,
      error: "Message Too Short"
    }; // If not, return an error

    // If we have got to this point, and not returned, the message must be good to go
    return { success: true };
  }


  router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "*");
    next();
  });
  router.use(bodyParser.json());


  router.get('/', function (req, res) {
    res.json({ success: true });
  })

  router.get('/login', function (req, res) {
    // Make the URL that the user needs to go to, to sign in with google
    var url = googleApi.getUrl();

    // Redirect them to it.
    res.redirect(url);
  })



  router.get('/login/callback', async function (req, res) {
    // This is the code that google gives back to us to say it was successful
    var code = req.query.code;

    // Look up the user's account details from the Google API
    var response = await googleApi.getAccount(code);


    // If user exists, fetch by google ID, otherwise make new user
    var user = await User.findOrCreate({
      where: { google_id: response.data.id }
    }, {
      google_id: response.data.id,
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none',
      teacher: response.data.hasOwnProperty("teacherFolder")
    });

    // Then, once we've found or created our user, look through the database again and update it to the newest details
    // In case the user already existed, but since we last saw them, they have changed their profile picture or name.
    await User.updateOne({
      where: { google_id: response.data.id }
    }, {
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none',
      teacher: response.data.hasOwnProperty("teacherFolder")
    })

    // Create the user a session and an API key
    var key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    // Store session key to database
    var session = await Session.create({
      key,
      user: user.id,
      created: Date.now()
    }).fetch();

    // Redirect the user to the frontend
    res.redirect(config.frontend_url + "/logincallback/" + session.key);
  })

  router.get('/login/test-login/student', async function (req, res) {
    if (config.env !== "dev") return res.json({ error: "Not Allowed" });
    // This is the code that google gives back to us to say it was successful
    var code = req.query.code;

    // Fake some account details
    var response = {
      data: {
        id: "i5TotwuCFZdNTZAb",
        email: "test-student@nbacademy.org.uk",
        name: "Test Student",
        display_name: "Test Student",
        picture: "https://img.icons8.com/plasticine/2x/user.png",
      },
      tokens: {
        access_token: "x",
        refresh_token: "x",
        id_token: "x"
      }
    };


    // If user exists, fetch by google ID, otherwise make new user
    var user = await User.findOrCreate({
      where: { google_id: response.data.id }
    }, {
      google_id: response.data.id,
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none'
    });

    // Then, once we've found or created our user, look through the database again and update it to the newest details
    // In case the user already existed, but since we last saw them, they have changed their profile picture or name.
    await User.updateOne({
      where: { google_id: response.data.id }
    }, {
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none'
    })

    // Create the user a session and an API key
    var key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    // Store session key to database
    var session = await Session.create({
      key,
      user: user.id,
      created: Date.now()
    }).fetch();

    // Redirect the user to the frontend
    res.redirect(config.frontend_url + "/logincallback/" + session.key);
  })

  router.get('/login/test-login/teacher', async function (req, res) {
    if (config.env !== "dev") return res.json({ error: "Not Allowed" });
    // This is the code that google gives back to us to say it was successful
    var code = req.query.code;

    // Fake some account details
    var response = {
      data: {
        id: "dsfjdskjfkds9",
        email: "test-teacher@nbacademy.org.uk",
        name: "Test Teacher",
        display_name: "Test Teacher",
        picture: "https://img.icons8.com/plasticine/2x/user.png"
      },
      tokens: {
        access_token: "x",
        refresh_token: "x",
        id_token: "x"
      }
    };


    // If user exists, fetch by google ID, otherwise make new user
    var user = await User.findOrCreate({
      where: { google_id: response.data.id }
    }, {
      google_id: response.data.id,
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none',
      teacher: true,
      leadership: true
    });

    // Then, once we've found or created our user, look through the database again and update it to the newest details
    // In case the user already existed, but since we last saw them, they have changed their profile picture or name.
    await User.updateOne({
      where: { google_id: response.data.id }
    }, {
      email: response.data.email,
      name: response.data.name,
      display_name: response.data.name,
      picture: response.data.picture,
      access_token: response.tokens.access_token,
      refresh_token: response.tokens.refresh_token,
      id_token: response.tokens.id_token,
      dyslexiaMode: 'none',
      teacher: true,
      leadership: true
    })

    // Create the user a session and an API key
    var key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    // Store session key to database
    var session = await Session.create({
      key,
      user: user.id,
      created: Date.now()
    }).fetch();

    // Redirect the user to the frontend
    res.redirect(config.frontend_url + "/logincallback/" + session.key);
  })


  // This method will run for every request after this point
  router.param("key", async function (req, res, proceed) {
    // Search the database for all sessions with the given API key. 
    // Then, when we find a session, also get the User associated with that session
    var sessions = await Session.find({ key: req.params.key }).populate("user");
    var session = sessions[0];

    // If we do not have any session returned, or if the session was created older than 3 days ago
    if ((session == undefined) || session.created < (Date.now() - (1000 * 60 * 60 * 24 * 3))) {
      // Then return that the request is not authorized
      res.status(403);
      res.end("Unauthorized");
      return;
    }

    // Else, set the variable `req.user` to be the user we just fetched
    req.user = session.user;
    // Then let the request continue
    proceed();
  })

  router.get("/:key/profile/me", async function (req, res) {
    // Return the user's own profile, in JSON format.
    res.json(req.user)
  })

  router.post("/:key/profile/me", async function (req, res) {
    // Update the user's own profile
    const res2 = await User.updateOne({
      google_id: req.user.google_id,
    }).set({
      dyslexiaMode: req.body.dyslexiaMode
    });
    res.json({ success: true, res2 })
  })

  router.get("/:key/courses", async function (req, res) {
    // For the test environment
    if (req.user.access_token === "x") return res.json(require("../example_data/classes.json"));

    // Contact the Google API to check what courses the user is in on google classroom
    var courses = await googleApi.listCourses(req.user);

    // Return the list of courses, JSON encoded.
    res.json(courses);
  })

  async function checkChannelPermissions(req, { id }) {
    const channel = await Channel.findOne({ id });
    if (!req.user.teacher && channel.type) return false;
    return true;
  }

  router.get("/:key/course/:course_id/channels", async function (req, res) {
    // Look for all channels in the class given in the URL
    var channels = await Channel.find({ class: req.params.course_id });

    let out = [];
    for (const channel of channels) {
      if (await checkChannelPermissions(req, channel)) out.push(channel);
    }

    for (const channel of out) {
      const pref = await NotificationPreference.findOne({ where: { channel: channel.id } });
      channel.notifications = pref ? pref.type : 'off';
    }

    // Return the list of channels with JSON encoded
    res.json(out);
  })

  router.post("/:key/course/:course_id/channel/:channel_id/notifications", async function (req, res) {
    // Create a channel in the database
    var a = await NotificationPreference.findOrCreate({
      where: { channel: req.params.channel_id, user: req.user.id }
    }, {
      channel: req.params.channel_id, 
      user: req.user.id,
      type: req.body.notifications
    })

    var b = await NotificationPreference.updateOne({
      where: { channel: req.params.channel_id, user: req.user.id }
    }, {
      channel: req.params.channel_id, 
      user: req.user.id,
      type: req.body.notifications
    })

    console.log(a,b);

    // Return a JSON encoded message saying it was successful
    res.json({ success: true });
  })

  router.get("/:key/course/:course_id/channel/:channel_id/messages", async function (req, res) {
    // Validate that the channel ID we were given does actually exist
    var channel = (await Channel.find({ class: req.params.course_id, id: parseInt(req.params.channel_id) }).limit(1))[0];
    if (!channel) return;

    if (!await checkChannelPermissions(req, channel)) return;

    // Look for all messages in that channel
    var messages = await Message.find({ channel: channel.id }).populate("sender").populate("file");

    // Return it with JSON encoding
    res.json(messages);
  })
  router.post("/:key/course/:course_id/channel/:channel_id/message/send", async function (req, res) {
    let message = { channel: req.params.channel_id, sender: req.user.id, timestamp: Math.round(Date.now() / 1000), content: req.body.content };

    if (!await checkChannelPermissions(req, { id: req.params.channel_id })) return;

    // Validate our message
    let validation = await validateMessage(req.params.course_id, message);

    // If the validation was not oK
    if (!validation.success) {
      return res.json({ error: true, message: validation.error });
    } else { // Else, it is fine
      // Create a message in the Messages table of the database
      const msg = await Message.create(message).fetch();
      notification.sendToChannel(req.params.channel_id, msg);

      // Return a JSON encoded message saying it was successful
      res.json({ error: false });
    }
  })

  router.post("/:key/report", async function (req, res) {
    console.log("Received report");
    await Report.create({
      message: req.body.message,
      reason: req.body.reason,
      notes: req.body.notes,
      reporter: req.body.includeName ? req.user.id : null
    });

    return res.json({ error: false });
  })

  router.post("/:key/auditlog", async function (req, res) {
    console.log("Received auditlog - " + JSON.stringify(req.body));
    await Auditlog.create({
      ...req.body,
      user: req.user.id
    });

    return res.json({ error: false });
  })

  router.post("/:key/course/:course_id/channel/:channel_id/message/sendfile/:file_id", async function (req, res) {
    // VALIDATE that file exists
    var file = (await File.find({ id: req.params.file_id }).limit(1))[0]
    if (!file) return res.json({ success: false, error: "File ID not found" });

    if (!await checkChannelPermissions(req, { id: req.params.channel_id })) return;

    // Create a message in the Messages table of the database
    await Message.create({
      channel: req.params.channel_id,
      sender: req.user.id,
      timestamp: Math.round(Date.now() / 1000),
      content: '',
      file: req.params.file_id
    });

    // Return a JSON encoded message saying it was successful
    res.json({ success: true });
  })

  router.post("/:key/notifications/add", async function (req, res) {
    // Create a channel in the database
    await NotificationDevice.findOrCreate({
      where: {
        token: req.body.token,
        user: req.user.id,
      }
    }, {
      token: req.body.token,
      user: req.user.id,
    });

    console.log(`Added notifications for user ${req.user.google_id} (token ${req.body.token})`)

    // Return a JSON encoded message saying it was successful
    res.json({ success: true });
  })

  // Leadership only
  const requireLeadership = (req, res, next) => {
    if (!req.user.leadership) return res.json({ success: false, error: "No Permission" });
    next();
  };
  // Teach only
  const requireTeacher = (req, res, next) => {
    if (!req.user.teacher) return res.json({ success: false, error: "No Permission" });
    next();
  };

  router.get("/:key/auditlog", requireLeadership, async function (req, res) {
    return res.json(await Auditlog.find({}).populate('user'));
  })

  router.post("/:key/course/:course_id/channel/create", requireTeacher, async function (req, res) {
    // Create a channel in the database
    const { id } = await Channel.create({
      name: req.body.name,
      class: req.params.course_id,
      type: req.body.type
    }).fetch();

    // Return a JSON encoded message saying it was successful
    res.json({ success: true, id });
  })

  router.post("/:key/course/:course_id/channel/:channel_id", requireTeacher, async function (req, res) {
    // Create a channel in the database
    const channel = await Channel
      .updateOne({ id: req.params.channel_id })
      .set(req.body.channel);

    // Return a JSON encoded message saying it was successful
    res.json({ success: true });
  })

  router.delete("/:key/course/:course_id/channel/:channel_id", requireTeacher, async function (req, res) {
    // Delete a channel in the database
    const channel = await Channel
      .destroyOne({ id: req.params.channel_id });

    // Return a JSON encoded message saying it was successful
    res.json({ success: true });
  })

  router.get("/:key/admin/reports", requireLeadership, async (req, res) => {
    var reports = await Report.find({}).populate("message").populate("reporter");
    var out = [];
    for (const report of reports) {
      report.message = (await Message.find({ id: report.message.id }).limit(1).populate("sender").populate("file").populate("channel"))[0]
      report.reporter = report.reporter ? (await User.find({ id: report.reporter.id }).limit(1))[0] : null;
      out.push(report);
    }
    res.json(out);
  })

  return { router, orm }; // Pass the initialised ORM back
}
module.exports.getRouter = getRouter;