var express = require('express')
var router = express.Router()
var googleApi = require("../util/google-api");
var dbHelper = require("../util/db");
var bodyParser = require('body-parser');
var fileUpload = require("express-fileupload");
var crypto = require("crypto");
var fs = require("fs");
var path = require("path");
var mime = require('mime-types')

const config = require("../config.json");
const { fstat } = require('fs');

async function getRouter(orm) {
    var File = orm.collections.file;


    router.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    router.use(fileUpload());
    router.use(bodyParser.json());

    router.post("/upload", async (req, res) => {
        if (!req.files || !req.files.file) {
            return res.json({ success: false, error: "No files uploaded" });
        }
        const id = crypto.randomBytes(16).toString('hex');
        console.log(`Upload => File ${req.files.file.name} - ID ${id}`);
        const file = await File.create({
            name: req.files.file.name,
            id,
            timestamp: Date.now()
        }).fetch();
        fs.writeFileSync(`${__dirname}/../storage/${id}`, req.files.file.data);
        res.json({ success: true, file });
    });

    router.get("/:id/*", async (req, res) => {
        var file = (await File.find({ id: req.params.id }).limit(1))[0]
        if (!file) return res.json({ success: false, error: "File ID not found" });
        res.set("Content-Type", mime.lookup(file.name));
        return res.sendFile(path.resolve(`${__dirname}/../storage/${file.id}`));
    });

    return router;
}
module.exports.getRouter = getRouter;