var DiskAdapter = require('sails-disk');
var Waterline = require('waterline');

// Used https://sailsjs.com/documentation/concepts/models-and-orm/standalone-waterline-usage for help with database models.

module.exports.getOrm = function () {
  return new Promise((resolve, reject) => {
    Waterline.start({
      // This tells the database to run from a file
      adapters: {
        'sails-disk': DiskAdapter,
      },
      datastores: {
        default: {
          adapter: 'sails-disk',
        }
      },

      // This defines the structure of the database and all of the fields
      models: {
        notificationDevice: {
          attributes: {
            token: { type: 'string', required: true },
            user: { model: 'user', required: true },
          }
        },
        notificationPreference: {
          attributes: {
            channel: { model: 'channel', required: true },
            user: { model: 'user', required: true },
            type: { type: 'string', required: true },
          }
        },
        user: {
          attributes: {
            google_id: { type: 'string', required: true },
            email: { type: 'string', required: true },
            name: { type: 'string', required: true },
            display_name: { type: 'string', required: false },
            picture: { type: 'string', required: true },
            access_token: { type: 'string', required: true },
            refresh_token: { type: 'string', required: true },
            id_token: { type: 'string', required: true },
            teacher: { type: 'boolean', defaultsTo: false },
            leadership: { type: 'boolean', defaultsTo: false },
            dyslexiaMode: { type: 'string', required: true }
          }
        },
        channel: {
          attributes: {
            class: { type: 'string', required: true },
            name: { type: 'string', required: true },
            type: { type: 'number', required: true }
          }
        },
        message: {
          attributes: {
            channel: { model: 'channel', required: true },
            sender: { model: 'user', required: true },
            timestamp: { type: 'number', meta: { columnType: 'bigint' }, required: true },
            content: { type: 'string', required: false },
            file: { model: 'file', required: false }
          }
        },
        session: {
          attributes: {
            key: { type: 'string', required: true },
            user: { model: 'user', required: true },
            created: { type: 'number', required: true },
          }
        },
        file: {
          attributes: {
            id: { type: 'string', required: true },
            name: { type: 'string', required: true },
            timestamp: { type: 'number', meta: { columnType: 'bigint' }, required: true }
          }
        },
        report: {
          attributes: {
            message: { model: 'message', required: true },
            notes: { type: 'string', required: false },
            reporter: { model: 'user', required: false },
            reason: { type: 'string', required: true }
          }
        },
        auditlog: {
          attributes: {
            message: { type: 'string', required: true },
            user: { model: 'user', required: true },
            time: { type: 'number', meta: { columnType: 'bigint' }, required: true },
          }
        },
      },
      defaultModelSettings: {
        attributes: {
          id: { type: 'number', autoMigrations: { autoIncrement: true } }, // Make an ID column on each table, and give it auto increment (so it goes up by itself)
        },
        primaryKey: 'id', // Make the `id` column of each model, it's primary key
        datastore: 'default', // Use the "default" datastore as defined above to be the disk storage
      }

    }, function (err, orm) {
      // If the ORM doesn't start up correctly, exit the app.
      if (err) {
        console.error('Could not start up the ORM:\n', err);
        return process.exit(1);
      }
      resolve(orm);
    });
  })
}
