module.exports = function (orm) {
    var NotificationDevice = orm.collections.notificationdevice;
    var NotificationPreference = orm.collections.notificationpreference;
    var Channel = orm.collections.channel;
    var User = orm.collections.user;

    // Log into firebase (cloud service for sending notifications)
    const admin = require("firebase-admin");
    const serviceAccount = require("./../educhat-1b4d2-firebase-adminsdk-bjcaf-905a24d0b7.json");
    const app = admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://educhat-1b4d2.firebaseio.com"
    });
    var exp = {};

    /**
     * Multicast a message to an array of users, to all of their known tokens
     * @param {Array} users Array of user IDs
     * @param {Object} notification Notification object
     */
    exp.sendToUsers = async function sendToUsers(users, notification) {
        const tk = await NotificationDevice.find({ where: { user: users } })
        console.log(`Sending notification to ${users.length} users (${tk.length} tokens) for "${notification.title}"`)
        admin.messaging().sendMulticast({
            notification,
            tokens: tk.map(t => t.token)
        })
    }

    exp.sendToChannel = async function sendToChannel(channel_id, message) {
        const registered = await NotificationPreference.find({ where: { channel: channel_id } });
        const channel = await Channel.findOne({ where: { id: channel_id } });

        console.log(registered)

        const send_to = [];

        // Only send notifications to people who have specifically registered for that channel
        for (const pref of registered) {
            const user = await User.findOne({ where: { id: pref.user } });

            // Dont send notification to sender of message
            if (user.id == message.sender) continue;

            // Send it if they have asked to see all notifications
            if (pref.type === "on") send_to.push(user.id);
            // Or if they want to see when they are mentioned, and their name is mentioned
            else if (pref.type === "mention" && message.content.includes(user.name)) send_to.push(user.id)
        }

        if (!send_to.length) return;

        const sender = await User.findOne({ where: { id: message.sender } });

        // Send the notification
        await exp.sendToUsers(send_to, {
            title: "New message in #" + channel.name,
            body: message.content,
            // Bug; Adding an icon breaks it
            //icon: sender.picture
        });
    }

    return exp
}

