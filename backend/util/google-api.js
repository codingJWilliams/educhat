var { google } = require("googleapis");
var config = require("../config.json");

// Used https://medium.com/@jackrobertscott/how-to-use-google-auth-api-with-node-js-888304f7e3a0 guide
// To help implement logging in with oauth

function getApi() {
  return new google.auth.OAuth2(
    config.google.clientId,
    config.google.clientSecret,
    config.google.redirect
  );
}

async function getAccount(code) {
  // Here, `code` is the code given to us when the user signs in with Google
  try {
    var auth = getApi();
    // Exchange our "code" for a set of login tokens
    var tokens = (await auth.getToken(code)).tokens;

    // Authenticate ourselves using these tokens
    auth.setCredentials(tokens);

    // Make a request to get the user's profile from google
    var oauth2 = google.oauth2({ version: 'v2', auth });
    var me = await oauth2.userinfo.v2.me.get();
    me.tokens = tokens;

    // Return the details
    return me;
  } catch (e) {
    // If there are any errors, pass the error on
    return { e }
  }
}

async function listCourses(tokens) {
  // In this case, tokens is the User model which has the important attributes `access_token`, `refresh_token` and `id_token` that are needed here

  // Get authenticated google api object
  var api = getApi();
  api.setCredentials(tokens);

  // Make the call to list the user's classes and return the resolt
  var classroom = google.classroom({ version: 'v1', auth: api });
  var courses = await classroom.courses.list({
    pageSize: 100,
  });
  return courses.data.courses;
}

function getUrl() {
  // This function generates the URL that the user needs to go to, to reach the sign in with google button

  // We request so many scopes for testing, so that I do not hit into permission errors
  // TODO: Narrow this down to only the ones needed
  var scopes = [
    'profile',
    'email',
    "https://www.googleapis.com/auth/classroom.courses", 
    "https://www.googleapis.com/auth/classroom.courses.readonly", 
    "https://www.googleapis.com/auth/classroom.profile.emails", 
    "https://www.googleapis.com/auth/classroom.profile.photos", 
  ];

  // Make the URL and return it
  var api = getApi();
  var url = api.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: scopes
  });
  return url;
}


// Any functions that are put here, can be accessed from outside this file.
module.exports = {
  getUrl,
  getAccount,
  listCourses,
}