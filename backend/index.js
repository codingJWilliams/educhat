const express = require('express')
const app = express()
const port = 3000

// Import our other files
var api = require("./routers/api.js");
api.getRouter().then(router => {
  // Register our router
  // The router decides which functions should be called depending on what request is made
  app.use("/api", router.router);
  console.log("Mounted API");

  var cdn = require("./routers/cdn.js");
  cdn.getRouter(router.orm).then(router => {
    // Register our router
    // The router decides which functions should be called depending on what request is made
    app.use("/cdn", router);
    console.log("Mounted CDN");
  })

})



// Start our application, listening on an http port
app.listen(port, () => console.log(`Listening on port ${port}!`));